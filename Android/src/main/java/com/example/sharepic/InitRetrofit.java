package com.example.sharepics;

import com.example.sharepic.sharePicApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InitRetrofit {

    private Retrofit retrofit;
    private static sharePicApi apiRouter;

    public InitRetrofit (){
        Gson gson = new GsonBuilder().setLenient().create();

        retrofit = new Retrofit.Builder().baseUrl("http://51.75.30.103:80/").addConverterFactory(GsonConverterFactory.create(gson)).build();
        apiRouter = retrofit.create(sharePicApi.class);
    }

    public static sharePicApi getApiRouter() {
        return apiRouter;
    }

    public static void setApiRouter(sharePicApi apiRouter) {
        InitRetrofit.apiRouter = apiRouter;
    }
}
