package com.example.sharepic;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.example.sharepics.InitRetrofit;

import java.net.URL;

public class UploadPictActivity extends AppCompatActivity {

    private Button newsButton;
    private Button profilButton;
    private Button cameraLogoButton;
    private Button cameraButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_upload_pict);

        newsButton = (Button) findViewById(R.id.newsButton);
        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentNews();
            }
        });
        profilButton = (Button) findViewById(R.id.profilButton);
        profilButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentProfil();
            }
        });

        cameraLogoButton = (Button) findViewById(R.id.logoButton);
        cameraLogoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCameraPermission() == 0)
                    intentCamera();
                else
                    askCameraPermission();
            }
        });

        cameraButton = (Button) findViewById(R.id.cameraButton);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCameraPermission() == 0)
                    intentCamera();
                else
                    askCameraPermission();
            }
        });
    }

    public void askCameraPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 100);
    }

    public int checkCameraPermission() {
        if (PermissionChecker.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            return 1;
        else
            return 0;
    }

    public void intentCamera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivity(intent);
    }

    public void intentProfil() {
        Intent intent = new Intent(this, ProfilActivity.class);
        startActivity(intent);
    }

    public void intentNews() {
        Intent intent = new Intent(this, NewsActivity.class);
        startActivity(intent);

    }
}
