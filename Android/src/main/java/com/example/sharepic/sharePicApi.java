package com.example.sharepic;

import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface sharePicApi {
    @FormUrlEncoded
    @POST("/Inscription")
    Call<JsonObject> register(@Field("firstname") String firstName, @Field("lastname") String lastName, @Field("email") String email, @Field("password") String password);

    @POST("/InsertPhoto")
    Call<JsonObject> insertPict(@Body Picture picture);

    /*/Connexion
    /InsertPhoto
    /RemovePhoto
    /GetPhoto GET
    /Like
    /Comment
    /FollowUser
    /UnFollow
    /GetFollowingUsers
    /GetNbFollowing
    /GetFollowingPhoto
    /GetFollowersUsers
    /GetNbFollowers*/
}
