package com.example.sharepic;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProfilActivity extends AppCompatActivity {

    private Button newsButton;
    private Button uploadPict;
    private Button cameraButton;

    private ListView listViewPict;
    private PictProfilAdapter pictProfilAdapter;

    private List<Picture> pictList;
    private Picture pict1;
    private Picture pict2;
    private Picture pict3;

    private List<String> comments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_profil);

        listViewPict = (ListView) findViewById(R.id.listView);

        pictList = setPictList();
        pictProfilAdapter = new PictProfilAdapter(getApplicationContext(), pictList);
        listViewPict.setAdapter(pictProfilAdapter);

        listViewPict.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                Picture picture1 = pictList.get(position);
                startActivity(picture1);
            }
        });



        newsButton = (Button) findViewById(R.id.newsButton);
        newsButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                intentNews();
            }
        });

        uploadPict = (Button) findViewById(R.id.uploadPictButton);
        uploadPict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentUpload();
            }
        });

        cameraButton = (Button) findViewById(R.id.logoButton);
        cameraButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                if(checkCameraPermission() == 0)
                    intentCamera();
                else
                    askCameraPermission();
            }
        });
    }

    private void startActivity(Picture picture){
        Intent intent = new Intent(this, PictureActivity.class);
        intent.putExtra("id", picture.getUrl());
        startActivity(intent);
    }

    private List<Picture> setPictList(){
        this.pict1 = new Picture();
        this.pict2 = new Picture();
        this.pict3 = new Picture();

        this.pict1.setDescription("MASUPERPREMIEREDESCRIPTION");
        this.pict1.setId("1");
        this.pict1.setUser("MONSUPERUSER1");
        this.pict1.setUrl("https://www.psychologue.net/site/articles/cf/cd/0/22334/selfi.jpg");
        this.pict1.setComment(Arrays.asList("sup1", "sup2", "sup3"));

        this.pict2.setDescription("MASUPERSECONDEDESCRIPTION");
        this.pict2.setId("2");
        this.pict2.setUser("MONSUPERUSER2");
        this.pict2.setUrl("https://www.sciencedaily.com/images/2018/09/180926140739_1_540x360.jpg");
        this.pict2.setComment(Arrays.asList("sup4", "sup5", "sup6"));

        this.pict3.setDescription("MASUPERSECONDEDESCRIPTION");
        this.pict3.setId("3");
        this.pict3.setUser("MONSUPERUSER3");
        this.pict3.setUrl("https://photos.lci.fr/images/613/344/benalla-senat-aac481-0@1x.jpeg");
        this.pict3.setComment(Arrays.asList("sup7", "sup8", "sup9"));

        List<Picture> pictureList = new ArrayList<>();
        pictureList.add(this.pict1);
        pictureList.add(this.pict2);
        pictureList.add(this.pict3);

        return pictureList;
    }

    public void askCameraPermission(){
        ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, 100);
    }

    public int checkCameraPermission(){
        if (PermissionChecker.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            return 1;
        else
            return 0;
    }

    public void intentCamera(){
        Intent intent = new Intent(this, UploadPictActivity.class);
        startActivity(intent);
    }

    public void intentUpload(){
        Intent intent = new Intent(this, UploadPictActivity.class);
        startActivity(intent);
    }

    public void intentNews(){
        Intent intent = new Intent(this, NewsActivity.class);
        startActivity(intent);
    }
}
