package com.example.sharepic;

import android.content.Context;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;

public class PictNewsAdapter extends BaseAdapter {
    private Context context;
    private List<Picture> pictures;

    public PictNewsAdapter(Context context, List<Picture> pictures){
        this.context = context;
        this.pictures = pictures;
    }

    @Override
    public Object getItem(int position) {
        return pictures.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return pictures.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.pict_news_row, null);
        }

        Picture picture = pictures.get(position);

        TextView pictName = (TextView) convertView.findViewById(R.id.pict_user);
        ImageView pict = (ImageView) convertView.findViewById(R.id.picture);
        TextView pictLike = (TextView) convertView.findViewById(R.id.pict_like);
        TextView pictDescription = (TextView) convertView.findViewById(R.id.pict_description);

        pictName.setText(picture.getUser());
        Picasso.get().load(picture.getUrl()).into(pict);
        pictLike.setText(picture.getLike());
        pictDescription.setText(picture.getDescription());

        return convertView;
    }
}
