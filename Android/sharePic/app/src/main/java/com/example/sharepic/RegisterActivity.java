package com.example.sharepic;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sharepics.InitRetrofit;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.sharepics.InitRetrofit.getApiRouter;

public class RegisterActivity extends AppCompatActivity{

    private Register register;
    private Button registerButton;
    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText password;

    private static InitRetrofit sharePicApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_register);
        getWindow().setBackgroundDrawableResource(R.mipmap.fond_login_register);

        sharePicApi = new InitRetrofit();

        registerButton = (Button) findViewById(R.id.register_button);
        registerButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                register();
            }
        });

        Button loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                intentLogin();
            }
        });
    }

    public void register(){
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);

        String firstNameStr = firstName.getText().toString();
        String lastNameStr = lastName.getText().toString();
        String emailStr = email.getText().toString();
        String passwordStr = password.getText().toString();

        register = new Register(firstNameStr, lastNameStr, emailStr, passwordStr);

        registerAPI(register);
    }

    public void intentLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void intentProfil(){
        Intent intent = new Intent(this, ProfilActivity.class);
        startActivity(intent);
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }


    public void registerAPI(Register register){
        Call<JsonObject> call = sharePicApi.getApiRouter().register(register.getFirstName(),register.getLastName(),register.getEmail(),register.getPassword());
        call.enqueue(new Callback<JsonObject>(){
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response){
                if(response.body() != null){
                    //Log.d(Tag, "onResponse "+response.body.toString());
                    intentLogin();
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t){
                intentProfil();
            }
        });
    }
}
