package com.example.sharepic;

import java.util.List;

public class Picture {
    private String url;
    private String user;
    private String id;
    private String description;
    private String like;
    private List<String> comments;

    public String getUrl(){
        return this.url;
    }
    public String getUser(){
        return this.user;
    }
    public String getId(){
        return this.id;
    }
    public String getDescription(){
        return this.description;
    }
    public String getLike(){
        return this.like;
    }
    public List<String> getComments(){
        return this.comments;
    }

    public void setLike(String like){
        this.like = like;
    }
    public void setUrl(String url){
        this.url = url;
    }
    public void setUser(String user){
        this.user = user;
    }
    public void setId(String id){
        this.id = id;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public void setComment(List<String> comments){
        this.comments = comments;
    }

}
