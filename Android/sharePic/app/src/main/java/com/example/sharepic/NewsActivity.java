package com.example.sharepic;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class NewsActivity extends AppCompatActivity {

    private Button profilButton;
    private Button cameraButton;

    private ListView listViewPict;
    private PictNewsAdapter pictNewsAdapter;

    private List<Picture> pictList;
    private Picture pict1;
    private Picture pict2;
    private Picture pict3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_news);

        listViewPict = (ListView) findViewById(R.id.listView);

        pictList = setPictList();
        this.pictNewsAdapter = new PictNewsAdapter(getApplicationContext(), pictList);
        this.listViewPict.setAdapter(pictNewsAdapter);

        listViewPict.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                Picture picture1 = pictList.get(position);
                startActivity(picture1);
            }
        });

        profilButton = (Button) findViewById(R.id.profilButton);
        profilButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentProfil();
            }
        });

        cameraButton = (Button) findViewById(R.id.logoButton);
        cameraButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                if(checkCameraPermission() == 0)
                    intentCamera();
                else
                    askCameraPermission();
            }
        });
    }

    private void startActivity(Picture picture){
        Intent intent = new Intent(this, PictureActivity.class);
        intent.putExtra("id", picture.getUrl());
        startActivity(intent);
    }

    private List<Picture> setPictList(){
        this.pict1 = new Picture();
        this.pict2 = new Picture();
        this.pict3 = new Picture();

        this.pict1.setDescription("MASUPERPREMIEREDESCRIPTION");
        this.pict1.setLike("16 Likes");
        this.pict1.setUser("User 1");
        this.pict1.setUrl("https://cdn.pixabay.com/photo/2013/04/06/11/50/image-editing-101040_960_720.jpg");

        this.pict2.setDescription("MASUPERSECONDEDESCRIPTION");
        this.pict2.setLike("21 Likes");
        this.pict2.setUser("User 2");
        this.pict2.setUrl("https://static.addtoany.com/images/dracaena-cinnabari.jpg");

        this.pict3.setDescription("MASUPERSECONDEDESCRIPTION");
        this.pict3.setLike("389 Likes");
        this.pict3.setUser("User 3");
        this.pict3.setUrl("https://media.koreus.com/201811/coeur-foret-arbre.jpg");

        List<Picture> pictureList = new ArrayList<>();
        pictureList.add(this.pict1);
        pictureList.add(this.pict2);
        pictureList.add(this.pict3);

        return pictureList;
    }

    public void askCameraPermission(){
        ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, 100);
    }

    public int checkCameraPermission(){
        if (PermissionChecker.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            return 1;
        else
            return 0;
    }

    public void intentProfil(){
        Intent intent = new Intent(this, ProfilActivity.class);
        startActivity(intent);
    }

    public void intentCamera(){
        Intent intent = new Intent(this, UploadPictActivity.class);
        startActivity(intent);
    }
}

