package com.example.sharepic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class CommentAdapter extends BaseAdapter {
    private Context context;
    private List<String> comments;

    public CommentAdapter(Context context, List<String> comments){
        this.context = context;
        this.comments = comments;
    }

    @Override
    public Object getItem(int position) {
        return comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.comment_row, null);
        }

        String comment = comments.get(position);

        TextView commentId = (TextView) convertView.findViewById(R.id.comment);

        commentId.setText(comment);

        return convertView;
    }
}
