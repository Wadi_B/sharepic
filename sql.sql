-- MySQL Script generated by MySQL Workbench
-- Wed Mar 27 15:03:47 2019
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`User` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Username` VARCHAR(45) NULL,
  `Firstname` VARCHAR(45) NULL,
  `Lastname` VARCHAR(45) NULL,
  `Email` VARCHAR(45) NULL,
  `Photo` TEXT NULL,
  `password` VARCHAR(255) NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Photo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Photo` (
  `idPhoto` INT NOT NULL AUTO_INCREMENT,
  `Description` TEXT NULL,
  `Date` DATETIME NULL,
  `url` TEXT NULL,
  `idUser` INT NULL,
  PRIMARY KEY (`idPhoto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Likes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Likes` (
  `idLikes` INT NOT NULL AUTO_INCREMENT,
  `idUser` INT NULL,
  `idPhoto` INT NULL,
  PRIMARY KEY (`idLikes`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Following`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Following` (
  `idFollowing` INT NOT NULL AUTO_INCREMENT,
  `idUser` INT NULL,
  `idFollowingUser` INT NULL,
  PRIMARY KEY (`idFollowing`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Follower`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Follower` (
  `idFollower` INT NOT NULL AUTO_INCREMENT,
  `idUser` INT NULL,
  `idFollowerUser` INT NULL,
  PRIMARY KEY (`idFollower`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Tag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Tag` (
  `idTag` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NULL,
  `Tagcol` VARCHAR(45) NULL,
  `idPhoto` INT NULL,
  PRIMARY KEY (`idTag`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Comment` (
  `idComment` INT NOT NULL AUTO_INCREMENT,
  `Content` TEXT NULL,
  `idUser` INT NULL,
  `idPhoto` INT NULL,
  PRIMARY KEY (`idComment`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`GPS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`GPS` (
  `idGPS` INT NOT NULL AUTO_INCREMENT,
  `GPS` TEXT NULL,
  `City` VARCHAR(255) NULL,
  `idPhoto` INT NULL,
  PRIMARY KEY (`idGPS`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
