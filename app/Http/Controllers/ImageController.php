<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;


class ImageController extends Controller
{
    function store($image) {
        $path = public_path() . "/images/";
        $imagepath = time() . "." . $image->getClientOriginalExtension();
        $image->move($path, $imagepath);
        return $path . $imagepath;
    }

    function remove($id, $path) {
        DB::table('Photo')->where('idPhoto', '=', $id)->delete();
        DB::table('GPS')->where('idPhoto', '=', $id)->delete();
        unlink($path);
        return true;
    }
}
