<?php

namespace App\Http\Controllers;
use App\UserDatabase;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;


class UserController extends Controller {
    function getUser($id) {
        $User = new UserDatabase();
        $User = $User->getUser($id);
        return response()->json(["success"=> $User[0], "error" => $User[1]],  $User[2]);
    }

    function getUserPictures(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $request = $request->all();
        $User = new UserDatabase();
        $User = $User->getUserPictures($request);
        return $User;
        return response()->json(["success"=> $User[0], "error" => $User[1]],  $User[2]);
    }
}

