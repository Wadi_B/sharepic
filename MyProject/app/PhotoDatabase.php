<?php

namespace App;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ImageController;


class PhotoDatabase {
    function InsertPhoto($Photo, $imagePath) {
        $Description = $Photo["Description"];
        $Date = $Photo["Date"];
        $idUser = $Photo["idUser"];
        $GPS = $Photo["GPS"];
        $url = $imagePath;
        DB::table('Photo')->insert(
            [
                'Description' => $Description,
                'Date' => $Date,
                'url' => $url,
                'idUser' => $idUser
            ]
        );
        $PhotoID = DB::table('Photo')->where('url', $url)->get()[0]->idPhoto;
        DB::table('GPS')->insert(
            [
                'idPhoto' => $PhotoID,
                'GPS' => $GPS,
            ]
        );
        return array(true, "none", 200);
    }

    function RemovePhoto($idPhoto) {
        $Photo = DB::table('Photo')->where('idPhoto', $idPhoto)->get();
        if (!isset($Photo[0])) {
            return array("false", "Photo doesn't exist", 400);
        }
        $Photo = $Photo[0];
        $path = $Photo->url;
        $ImageCon = new ImageController();
        $Imageremove = $ImageCon->remove($idPhoto, $path);
        return array($Imageremove, "none", 200);
    }
}