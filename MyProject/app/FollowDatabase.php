<?php

namespace App;
use Illuminate\Support\Facades\DB;


class FollowDatabase {
    function Follow($request) {
        $idUser = $request["idUser"];
        $idFollow = $request["idFollow"];
        $exist = $this->exist($idUser, $idFollow);
        if ($exist) {
            $response = array(false, "Already following", 400);
            return $response;
        }
        $insert = DB::table('Following')->insert(
            [
                'idUser' => $idUser,
                'idFollowingUser' => $idFollow,
            ]
        );
        return $this->CheckReturn($insert);
    }

    function unFollow($request) {
        $idUser = $request["idUser"];
        $idFollow = $request["idFollow"];
        $exist = $this->exist($idUser, $idFollow);
        if (!$exist) {
            $response = array(false, "Cannot be deleted", 400);
            return $response;
        }
        $delete = DB::table('Following')->where([
            ['idUser', '=', $idUser],
            ['idFollowingUser', '=', $idFollow],
        ])
        ->delete();
        return $this->CheckReturn($delete);
    }

    // ---------------- Following ---------------- //

    function GetFollowing($request) {
        $idUser = $request["idUser"];
        $Following = DB::table('Following')
        ->Join('User', 'idFollowingUser', '=', 'ID')
        ->select("Username", "ID", "idUser")
        ->where('idUser', $idUser)
        ->get();
        $response = array($Following, false, 200);
        return $response;
    }

    function GetNbFollowing($request) {
        $idUser = $request["idUser"];
        $NbFollowing = DB::table('Following')
        ->Join('User', 'idFollowingUser', '=', 'ID')
        ->select(DB::raw("COUNT(ID) as nb_following"))
        ->where('idUser', $idUser)
        ->get();
        $response = array($NbFollowing, "none", 200);
        return $response;
    }

    // ---------------- Followers ---------------- //

    function GetFollowers($request) {
        $idUser = $request["idUser"];
        $Following = DB::table('Following')
        ->Join('User', 'idFollowingUser', '=', 'ID')
        ->select("Username", "ID", "idUser")
        ->where('idFollowingUser', $idUser)
        ->get();
        $response = array($Following, "none", 200);
        return $response;
    }

    function GetNbFollowers($request) {
        $idUser = $request["idUser"];
        $NbFollowing = DB::table('Following')
        ->Join('User', 'idFollowingUser', '=', 'ID')
        ->select(DB::raw("COUNT(ID) as nb_following"))
        ->where('idFollowingUser', $idUser)
        ->get();
        $response = array($NbFollowing, "none", 200);
        return $response;
    }

    // ---------------- Get Following Photo ---------------- //

    function GetFollowingPhoto($request) {
        $idUser = $request["idUser"];
        $FollowingPhoto = DB::table('Following')
        ->Join('Photo', 'idFollowingUser', '=', 'Photo.idUser')
        ->select("idFollowingUser", "Photo.url")
        ->where('Following.idUser', $idUser)
        ->get();
        $response = array($FollowingPhoto, false, 200);
        return $response;
    }

    // ---------------- Utils ---------------- //

    function CheckReturn($return) {
        if ($return) {
            $response = array(true, "none", 200);
            return $response;
        }
        else {
            $response = array(false, "Unexpected error", 400);
            return $response;
        }
    }

    function exist($idUser, $idFollow) {
        $exist = DB::table('Following')->where([
            ['idUser', '=', $idUser],
            ['idFollowingUser', '=', $idFollow],
        ])
        ->exists();
        return $exist;
    }
}