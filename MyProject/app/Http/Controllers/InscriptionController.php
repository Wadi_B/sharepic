<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\UserDatabase;


class InscriptionController extends Controller
{
    public function Inscription(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'Username' => 'required',
            'Firstname' => 'required',
            'Lastname' => 'required',
            'Email' => 'required',
            'Photo' => 'required|image',
            'Password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $User = $request->all();
        $image = $request->file("Photo");
        $ImageCon = new ImageController();
        $Imagepath = $ImageCon->store($image);
        $Userfun = new UserDatabase();
        $Userfun = $Userfun->register($User, $Imagepath);
        return response()->json(["success"=> $Userfun[0], "error" => $Userfun[1]], $Userfun[2]);
    }

    public function Connexion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'Username' => 'required',
            'Password' => 'required',
        ]);
        $User = $request->all();
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $Userfun = new UserDatabase();
        $Userfun = $Userfun->login($User);
        return response()->json(["success"=> $Userfun[0], "error" => $Userfun[1]], $Userfun[2]);
    }
}