<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\FollowDatabase;


class FollowController extends Controller {
    function Follow(Request $request) {
        $validator = Validator::make($request->all(), [
            'idUser' => 'required',
            'idFollow' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $data = $request->all();
        $Follow = new FollowDatabase();
        $Follow = $Follow->Follow($data);
        return response()->json(["success"=> $Follow[0], "error" => $Follow[1]], $Follow[2]);
    }

    function UnFollow(Request $request) {
        $validator = Validator::make($request->all(), [
            'idUser' => 'required',
            'idFollow' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $data = $request->all();
        $Follow = new FollowDatabase();
        $Follow = $Follow->UnFollow($data);
        return response()->json(["success"=> $Follow[0], "error" => $Follow[1]], $Follow[2]);
    }

    // ---------------- Following ---------------- //

    function GetFollowing(Request $request) {
        $validator = Validator::make($request->all(), [
            'idUser' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $data = $request->all();
        $Follow = new FollowDatabase();
        $Follow = $Follow->GetFollowing($data);
        return response()->json(["success"=> $Follow[0], "error" => $Follow[1]], $Follow[2]);
    }

    function GetNbFollowing(Request $request) {
        $validator = Validator::make($request->all(), [
            'idUser' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $data = $request->all();
        $Follow = new FollowDatabase();
        $Follow = $Follow->GetNbFollowing($data);
        return response()->json(["success"=> $Follow[0], "error" => $Follow[1]], $Follow[2]);
    }

    // ---------------- Followers ---------------- //

    function GetFollowers(Request $request) {
        $validator = Validator::make($request->all(), [
            'idUser' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $data = $request->all();
        $Follow = new FollowDatabase();
        $Follow = $Follow->GetFollowers($data);
        return response()->json(["success"=> $Follow[0], "error" => $Follow[1]], $Follow[2]);
    }

    function GetNbFollowers(Request $request) {
        $validator = Validator::make($request->all(), [
            'idUser' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $data = $request->all();
        $Follow = new FollowDatabase();
        $Follow = $Follow->GetNbFollowers($data);
        return response()->json(["success"=> $Follow[0], "error" => $Follow[1]], $Follow[2]);
    }

    // ---------------- Get Following Photo ---------------- //

    function GetFollowingPhoto(Request $request) {
        $validator = Validator::make($request->all(), [
            'idUser' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $data = $request->all();
        $Follow = new FollowDatabase();
        $Follow = $Follow->GetFollowingPhoto($data);
        return response()->json(["success"=> $Follow[0], "error" => $Follow[1]], $Follow[2]);
    }


}
