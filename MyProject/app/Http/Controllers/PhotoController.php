<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\PhotoDatabase;


class PhotoController extends Controller
{
    function InsertPhoto(Request $request) {
        $validator = Validator::make($request->all(), [
            'Photo' => 'required',
            'Description' => 'required',
            'Date' => 'required',
            'idUser' => 'required',
            'GPS' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $Photo = $request->all();
        $image = $request->file("Photo");
        $ImageCon = new ImageController();
        $Imagepath = $ImageCon->store($image);
        $PhotoDB = new PhotoDatabase();
        $PhotoDB = $PhotoDB->InsertPhoto($Photo, $Imagepath);
        return response()->json(["success"=> $PhotoDB[0], "error" => $PhotoDB[1]], $PhotoDB[2]);

    }

    function RemovePhoto(Request $request) {
        $validator = Validator::make($request->all(), [
            'idPhoto' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $idPhoto = $request->idPhoto;
        $PhotoDB = new PhotoDatabase();
        $PhotoDB = $PhotoDB->RemovePhoto($idPhoto);
        return response()->json(["success" => $PhotoDB[0], "error" => $PhotoDB[1]], $PhotoDB[2]);
    }
}
