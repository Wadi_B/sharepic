<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\ReactionDatabase;


class ReactionController extends Controller
{
    function GetPhoto($id) {
        $Reaction = new ReactionDatabase();
        return $Reaction->GetPhoto($id);
    }

    function isLiked(Request $request) {
        $data = $request->all();
        $Reaction = new ReactionDatabase();
        $Reaction = $Reaction->isLiked($data);
        return response()->json(["success"=> $Reaction[0], "error" => $Reaction[1]], $Reaction[2]);
    }

    function Like(Request $request) {
        $validator = Validator::make($request->all(), [
            'idPhoto' => 'required',
            'idUser' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $data = $request->all();
        $Reaction = new ReactionDatabase();
        $Reaction =  $Reaction->Like($data);
        return response()->json(["success"=> $Reaction[0], "error" => $Reaction[1]], $Reaction[2]);
    }

    function Comment(Request $request) {
        $validator = Validator::make($request->all(), [
            'idPhoto' => 'required',
            'idUser' => 'required',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=> false, "error" => $validator->errors()->first()], 400);
        }
        $data = $request->all();
        $Reaction = new ReactionDatabase();
        $Reaction =  $Reaction->Comments($data);
        return response()->json(["success"=> $Reaction[0], "error" => $Reaction[1]], $Reaction[2]);
    }
}
