<?php

namespace App;
use Illuminate\Support\Facades\DB;


class UserDatabase {
    function getUser($id) {
        $User = DB::table('User')->where('ID', $id)->get();
        if (isset($User[0])) {
            return array($User[0], false, 200);
        }
        else {
            return $this->getUserByUsername($id);
        }
    }

    function getUserByUsername($Username) {
        $User = DB::table('User')->where('Username', $Username)->get();
        if (isset($User[0])) {
            return array($User[0], false, 200);
        }
        else {
            return array(false, "User not found", 404);
        }
    }

    function register($User, $imagePath) {
        $Username = $User["Username"];
        $Firstname = $User["Firstname"];
        $Lastname = $User["Lastname"];
        $Email = $User["Email"];
        $Photo = $imagePath;
        $Password = $User["Password"];
        $Password = password_hash($Password, PASSWORD_DEFAULT);
        $query =  DB::table('User')->where('Email', $Email)->exists();

        if ($query) {
            return array(false, "Email already exist", 401);
        }
        DB::table('User')->insert(
            [
                'Username' => $Username,
                'Firstname' => $Firstname,
                'Lastname' => $Lastname,
                'Email' => $Email,
                'Photo' =>  $Photo,
                'Password' => $Password
            ]
        );
        return (true);
    }

    function login($User) {
        $Username = $User["Username"];
        $Password = $User["Password"];
        if (!isset(DB::table('User')->where('Username', $Username)->get()[0])) {
            return array(false, "Username and password do not match", 403);
        }
        $PasswordDB = $UserDB->password;
        $verif = password_verify($Password , $PasswordDB);
        if($verif == true) {
            return array(true, "none", 200);
        }
    }

    function getUserPictures($request) {
        $id = $request["id"];
        $begin = 0;
        $nb = 10;
        if (isset($request["begin"])) {
            $begin = $request["begin"];
        }
        if (isset($request["nb"])) {
            $nb = $request["nb"];
        }
        $Photo = DB::table('Photo')
        ->where('idUser', $id)
        ->orderBy('Date','desc')
        ->skip($begin)->take($nb)
        ->get();

        // if(isset($Photo)) {
        //     $Photo = $Photo[0];
        // }
        return $Photo;
    }

}