<?php

namespace App;
use Illuminate\Support\Facades\DB;


class ReactionDatabase {

    // ---------------- Get Infos  ---------------- //

    function GetPhoto($id) {
        if (!(DB::table('Photo')->where("idPhoto", "=", $id)->exists())) {
            return array(false, "Photo does not exists", 404);
        }
        $Comments = DB::table('Photo')
        ->leftJoin('Comment', 'Photo.idPhoto', '=', 'Comment.idPhoto')
        ->select("Photo.idPhoto", "Photo.url", "Comment.content", "Photo.idUser", "Photo.Description", "Comment.idUser")
        ->where('Photo.idPhoto', $id)
        ->get();

        $Photo = array();
        $comments = array();

        if (isset($Comments[0])) {
            foreach($Comments as $c) {
                $url = $c->url;
                $idUser = $c->idUser;
                $Description = $c->Description;
                array_push($comments, $c->content);
                $Photo = ['url' => $url, 'idUser' => $idUser, 'Description' => $Description, 'comments' => $comments];
            }
        }
        $Likes = DB::table('Photo')
        ->leftJoin('Likes', 'Photo.idPhoto', '=', 'Likes.idPhoto')
        ->select(DB::raw("COUNT(Likes.idUser) as nb_like"))
        ->where('Photo.idPhoto', $id)
        ->get();
        if(isset($Likes[0])) {
            $Likes =['Likes' => $Likes[0]->nb_like];
            $array = array_merge($Photo, $Likes);
            json_encode($array);
        }
        return $array;
    }

    // ---------------- Like ---------------- //

    function Like($request) {
        $idUser = $request["idUser"];
        $idPhoto = $request["idPhoto"];

        if (!(DB::table('Likes')->where("idPhoto", "=", $idPhoto)->exists())) {
            return array(false, "Photo does not exists", 404);
        }
        if ($this->exist($idUser, $idPhoto)) {
            return array(false, "Photo already liked", 401);
        }
        $insert = DB::table('Likes')->insert(
            [
                'idUser' => $idUser,
                'idPhoto' => $idPhoto,
            ]
        );
        return $this->CheckReturn($insert);
    }

    function isLiked($data) {
        $idUser =  $data["idUser"];
        $idPhoto =  $data["idPhoto"];
        if ($this->exist($idUser, $idPhoto)) {
            return array(true, "none", 200);
        }
        return array(false, "none", 200);
    }

    // ---------------- Comment ---------------- //

    function Comments($request) {
        $idUser = $request["idUser"];
        $idPhoto = $request["idPhoto"];
        $content = $request["content"];

        if (!(DB::table('Likes')->where("idPhoto", "=", $idPhoto)->exists())) {
            return array(false, "Photo does not exist", 404);
        }
        $insert = DB::table('Comment')->insert(
            [
                'idUser' => $idUser,
                'idPhoto' => $idPhoto,
                'content' => $content,
            ]
        );
        return $this->CheckReturn($insert);
    }

    // ---------------- Utils ---------------- //

    function CheckReturn($return) {
        if ($return) {
            $response = array(true, "none", 200);
            return $response;
        }
        else {
            $response = array(false, "Unexpected error", 400);
            return $response;
        }
    }

    function exist($idUser, $idPhoto) {
        $exist = DB::table('Likes')->where([
            ['idUser', '=', $idUser],
            ['idPhoto', '=', $idPhoto],
        ])
        ->exists();
        return $exist;
    }



}
