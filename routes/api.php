<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/getUser/{Id}', 'UserController@getUser');
Route::post('/getUserPictures', 'UserController@getUserPictures');
Route::get('/getUserByUsername/{Username}', 'UserController@getUserByUsername');

Route::post('/Inscription', 'InscriptionController@Inscription');
Route::post('/Connexion', 'InscriptionController@Connexion');

Route::post('/InsertPhoto', 'PhotoController@InsertPhoto');
Route::post('/RemovePhoto', 'PhotoController@RemovePhoto');

Route::get('/GetPhoto/{Id}', 'ReactionController@GetPhoto');
Route::post('/Like', 'ReactionController@Like');
Route::post('/Comment', 'ReactionController@Comment');
Route::post('/isLiked', 'ReactionController@isLiked');




Route::post('/FollowUser', 'FollowController@Follow');
Route::post('/UnFollowUser', 'FollowController@UnFollow');

Route::post('/GetFollowingUsers', 'FollowController@GetFollowing');
Route::post('/GetNbFollowing', 'FollowController@GetNbFollowing');
Route::post('/GetFollowingPhoto', 'FollowController@GetFollowingPhoto');


Route::post('/GetFollowersUsers', 'FollowController@GetFollowers');
Route::post('/GetNbFollowers', 'FollowController@GetNbFollowers');



